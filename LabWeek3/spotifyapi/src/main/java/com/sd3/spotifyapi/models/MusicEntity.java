package com.sd3.spotifyapi.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="TB_MUSICS")
public class MusicEntity extends AbstractEntity{

    @Column(name ="TL_MUSIC")
    @Getter
    @Setter
    private String title;

    @Column(name="ART_MUSIC")
    @Getter
    @Setter
    private String artist;

    @Column(name="GED_MUSIC")
    @Getter
    @Setter
    private String gender;

    @Column(name="DUR_MUSIC")
    @Getter
    @Setter
    private Double duration;

}
