package com.sd3.spotifyapi.exceptions;

public class MusicNotFoundException extends RuntimeException{
    public MusicNotFoundException(String message) {
        super(message);
    }

    public MusicNotFoundException() {
        super("Music not found.");
    }

}
