package com.sd3.spotifyapi.controllers;

import com.sd3.spotifyapi.dtos.MusicRecordDTO;
import com.sd3.spotifyapi.models.MusicEntity;
import com.sd3.spotifyapi.services.MusicService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
public class MusicController {

    private final MusicService musicService;

    @Autowired
    public MusicController(MusicService musicService) {
        this.musicService = musicService;
    }

    @PostMapping("/musics")
    public ResponseEntity<MusicEntity> createMusic(@RequestBody @Valid MusicRecordDTO music){
        return musicService.saveMusic(music);
    }

    @DeleteMapping("/musics/{id}")
    public ResponseEntity<Object> deleteMusic(@PathVariable(value = "id") UUID id){
        return musicService.deleteMusic(id);
    }

    @PatchMapping("/musics/{id}")
    public ResponseEntity<Object> updateMusic(@RequestBody @Valid Map<String, String> fieldsForUpdate, @PathVariable(value = "id") UUID id){
        return musicService.updateMusic(fieldsForUpdate, id);
    }

    @GetMapping("/musics")
    public ResponseEntity<List<MusicEntity>> getAllMusics(){
        return musicService.getAllMusics();
    }

    @GetMapping("/musics/{id}")
    public ResponseEntity<Object> getMusicByID(@PathVariable(value = "id") UUID id){
        return musicService.getMusicByID(id);
    }

    @GetMapping("/musics/{param}/{value}")
    public ResponseEntity<Object> getMusicsByParam(@PathVariable(value = "param") String param, @PathVariable(value = "value") String value){
        return musicService.getMusicsByParam(param, value);
    }


}

