package com.sd3.spotifyapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpotifyapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpotifyapiApplication.class, args);
	}

}
