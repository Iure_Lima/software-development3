package com.sd3.spotifyapi.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@MappedSuperclass
public abstract class AbstractEntity extends RepresentationModel<AbstractEntity> implements Serializable {

    AbstractEntity(){
        Date now = new Date();
        created_at = now;
        update_at = now;

    }

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name="ID")
    @Getter
    private UUID id;

    @Column(name="DT_CREATED_AT")
    @Getter
    private final Date created_at;

    @Column(name="DT_UPDATED_AT")
    @Getter
    @Setter
    private Date update_at;













}
