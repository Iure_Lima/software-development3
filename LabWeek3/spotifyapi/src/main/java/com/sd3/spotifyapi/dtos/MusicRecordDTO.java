package com.sd3.spotifyapi.dtos;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record MusicRecordDTO(@NotBlank String title, @NotBlank String artist, @NotBlank String gender, @NotNull Double duration) {
}
