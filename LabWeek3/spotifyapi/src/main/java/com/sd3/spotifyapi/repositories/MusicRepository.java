package com.sd3.spotifyapi.repositories;

import com.sd3.spotifyapi.models.MusicEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface MusicRepository extends JpaRepository<MusicEntity, UUID> {

    @Query("SELECT m FROM MusicEntity m WHERE m.title LIKE :title%")
    List<MusicEntity> findByTitle(@Param("title") String title);

    List<MusicEntity> findByArtist(String artist);

    List<MusicEntity> findByGender(String gender);

}
