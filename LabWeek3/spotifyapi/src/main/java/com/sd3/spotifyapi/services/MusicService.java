package com.sd3.spotifyapi.services;

import com.sd3.spotifyapi.controllers.MusicController;
import com.sd3.spotifyapi.dtos.MusicRecordDTO;
import com.sd3.spotifyapi.exceptions.MusicNotFoundException;
import com.sd3.spotifyapi.models.MusicEntity;
import com.sd3.spotifyapi.repositories.MusicRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Service
public class MusicService {

    private final MusicRepository musicRepository;

    @Autowired
    public MusicService(MusicRepository musicRepository) {
        this.musicRepository = musicRepository;
    }

    public ResponseEntity<MusicEntity> saveMusic(MusicRecordDTO musicRecordDTO){
        MusicEntity musicEntity = new MusicEntity();
        BeanUtils.copyProperties(musicRecordDTO, musicEntity);
        return ResponseEntity.status(HttpStatus.CREATED).body(musicRepository.save(musicEntity));
    }

    public ResponseEntity<Object> deleteMusic(UUID id){
        Optional<MusicEntity> music = musicRepository.findById(id);

        if(music.isEmpty()) throw new MusicNotFoundException("Music Not Found. Not Possible To Delete.");
        musicRepository.delete(music.get());

        return ResponseEntity.status(HttpStatus.OK).body("Music deleted successfully");
    }

    public ResponseEntity<Object> updateMusic(Map<String, String> fieldForUpdate, UUID id){
        Optional<MusicEntity> originalMusic = musicRepository.findById(id);

        if (originalMusic.isEmpty()) throw new MusicNotFoundException("Music Not Found. Not Possible To Delete.");

        MusicEntity music = originalMusic.get();

        if (fieldForUpdate.containsKey("title")) music.setTitle(fieldForUpdate.get("title"));

        if (fieldForUpdate.containsKey("artist")) music.setArtist(fieldForUpdate.get("artist"));

        if (fieldForUpdate.containsKey("gender")) music.setGender(fieldForUpdate.get("gender"));

        if (fieldForUpdate.containsKey("duration")) music.setDuration(Double.parseDouble(fieldForUpdate.get("duration")));

        music.setUpdate_at(new Date());

        return ResponseEntity.status(HttpStatus.OK).body(musicRepository.save(music));
    }

    public ResponseEntity<List<MusicEntity>> getAllMusics(){
        List<MusicEntity> listMusics = musicRepository.findAll();

        if (!listMusics.isEmpty()) {
            for (MusicEntity music : listMusics){
                UUID id = music.getId();
                music.add(linkTo(methodOn(MusicController.class).getMusicByID(id)).withSelfRel());
            }
        }

        return ResponseEntity.status(HttpStatus.OK).body(listMusics);
    }

    public ResponseEntity<Object> getMusicByID(UUID id){
        Optional<MusicEntity> music = musicRepository.findById(id);

        if(music.isEmpty()) throw new MusicNotFoundException("Music Not Found. Not Possible To Read.");

        music.get().add(linkTo(methodOn(MusicController.class).getAllMusics()).withRel("Musics List"));

        return ResponseEntity.status(HttpStatus.OK).body(music.get());
    }

    public ResponseEntity<Object> getMusicsByParam(String param, String value){

        List<MusicEntity> musics = getMusicsByDB(param,value);

        if (musics.isEmpty()) throw new MusicNotFoundException("Music Not Found. Not Possible To Read.");

        for (MusicEntity music : musics){
            UUID id = music.getId();
            music.add(linkTo(methodOn(MusicController.class).getMusicByID(id)).withSelfRel());
        }

        return ResponseEntity.status(HttpStatus.OK).body(musics);
    }

    public List<MusicEntity> getMusicsByDB(String param, String value){
        if (param.toLowerCase().equals("title")) return musicRepository.findByTitle(value);

        if (param.toLowerCase().equals("artist")) return musicRepository.findByArtist(value);

        if (param.toLowerCase().equals("gender")) return musicRepository.findByGender(value);

        return new ArrayList<>();
    }
}
