package com.sd3.spotifyapi.utils;

import com.sd3.spotifyapi.exceptions.MusicNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(MusicNotFoundException.class)
    private ResponseEntity<RestErrorMessage> musicNotFoundHandler(MusicNotFoundException musicNotFoundException){
        RestErrorMessage errorMessage = new RestErrorMessage(HttpStatus.NOT_FOUND,musicNotFoundException.getMessage());
        return ResponseEntity.status(errorMessage.getStatus()).body(errorMessage);
    }

    @ExceptionHandler(RuntimeException.class)
    private ResponseEntity<RestErrorMessage> exceptionHandler(Exception exception){
        RestErrorMessage errorMessage = new RestErrorMessage(HttpStatus.BAD_REQUEST, exception.getMessage());
        return ResponseEntity.status(errorMessage.getStatus()).body(errorMessage);

    }


}
