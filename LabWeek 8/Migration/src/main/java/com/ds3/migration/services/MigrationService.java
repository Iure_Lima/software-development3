package com.ds3.migration.services;

import com.ds3.migration.models.ProductEntity;
import com.ds3.migration.models.ProductEntityMongo;
import com.ds3.migration.repositories.ProductRepository;
import com.ds3.migration.repositories.ProductRepositoryMongo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MigrationService {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    ProductRepositoryMongo productRepositoryMongo;

    public void migration(){
        List<ProductEntity> productEntities = productRepository.findAll();
        for (ProductEntity product:productEntities){
            ProductEntityMongo productMongo = new ProductEntityMongo(
                    product.getId().toString(),
                    product.getName(),
                    product.getPrice(),
                    product.getStock(),
                    product.getCreatedAt(),
                    product.getUpdatedAt());

            productRepositoryMongo.save(productMongo);
        }
    }
}
