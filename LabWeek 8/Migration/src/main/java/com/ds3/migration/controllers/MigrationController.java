package com.ds3.migration.controllers;

import com.ds3.migration.services.MigrationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/migration")
@CrossOrigin(origins = "*", maxAge = 3600)
@Tag(name = "Migration-Controller")
public class MigrationController {
    @Autowired
    MigrationService migrationService;

    @PostMapping
    @Operation(summary = "Migra os dados do PostgresSQL para MongoDB", method = "POST")
    public ResponseEntity<String> createdProduct(){
        migrationService.migration();
        return ResponseEntity.status(HttpStatus.OK).body("Migration complete");
    }
}
