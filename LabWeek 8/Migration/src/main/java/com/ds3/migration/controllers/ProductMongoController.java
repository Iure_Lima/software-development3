package com.ds3.migration.controllers;

import com.ds3.migration.models.ProductEntity;
import com.ds3.migration.models.ProductEntityMongo;
import com.ds3.migration.services.ProductMongoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/products-mongo")
@CrossOrigin(origins = "*", maxAge = 3600)
@Tag(name = "Mongo-Product-Controller")
public class ProductMongoController {

    @Autowired
    ProductMongoService productMongoService;

    @GetMapping
    @Operation(summary = "Busca os produtos do MongoDB", method = "GET")
    public ResponseEntity<List<ProductEntityMongo>> getProducts(){
        return ResponseEntity.status(HttpStatus.OK).body(productMongoService.getAllProducts());
    }
}
