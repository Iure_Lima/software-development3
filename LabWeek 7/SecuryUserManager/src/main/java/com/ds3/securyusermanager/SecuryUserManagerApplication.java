package com.ds3.securyusermanager;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "SecurityUserManager", version = "1.0.0", description = "API de aprendizado do spring boot"))
public class SecuryUserManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecuryUserManagerApplication.class, args);
	}

}
