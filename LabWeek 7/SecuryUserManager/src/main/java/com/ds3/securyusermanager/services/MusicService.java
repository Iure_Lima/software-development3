package com.ds3.securyusermanager.services;

import com.ds3.securyusermanager.models.DTOs.MusicRequestDTO;
import com.ds3.securyusermanager.models.MusicEntity;
import com.ds3.securyusermanager.repositories.MusicRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class MusicService {

    @Autowired
    private MusicRepository repository;

    public MusicEntity saveMusic(MusicRequestDTO musicDTO){
        MusicEntity musicEntity = new MusicEntity();
        BeanUtils.copyProperties(musicDTO, musicEntity);
        return repository.save(musicEntity);
    }

    public List<MusicEntity> getAllMusics(){
        return repository.findAll();
    }

    public Optional<MusicEntity> getById(UUID id){
        Optional<MusicEntity> music = repository.findById(id);
        return music;
    }

    public Object delete(UUID id){
        Optional<MusicEntity> music = repository.findById(id);
        if (music.isEmpty()){
            return "Music Not Found";
        }
        repository.delete(music.get());
        return "Music deleted with sucess";
    }

    public Object update(MusicRequestDTO musicRequestDTO, UUID id){
        Optional<MusicEntity> music = repository.findById(id);
        if (music.isEmpty()){
            return "Music Not Found";
        }
        MusicEntity newMusic = music.get();
        BeanUtils.copyProperties(musicRequestDTO,newMusic);
        newMusic.setUpdatedAt(new Date());
        return repository.save(newMusic);
    }
}
