package com.ds3.securyusermanager.controllers;

import com.ds3.securyusermanager.models.DTOs.MusicRequestDTO;
import com.ds3.securyusermanager.models.MusicEntity;
import com.ds3.securyusermanager.services.MusicService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/music")
@CrossOrigin(origins = "*", maxAge =3600 )
@Tag(name = "SecurityAPI")
public class MusicController {

    @Autowired
    private MusicService musicService;

    @PostMapping
    @Operation(summary = "Salva música", method = "POST")
    public ResponseEntity<MusicEntity> save(@RequestBody @Valid MusicRequestDTO musicRequestDTO){
        return ResponseEntity.status(HttpStatus.CREATED).body(musicService.saveMusic(musicRequestDTO));
    }

    @GetMapping
    @Operation(summary = "Busca todas as músicas", method = "GET")
    public ResponseEntity<List<MusicEntity>> getAll(){
        return ResponseEntity.status(HttpStatus.OK).body(musicService.getAllMusics());
    }

    @GetMapping("/{id}")
    @Operation(summary = "Busca todas as músicas", method = "GET")
    public ResponseEntity<Object> getById(@PathVariable(value = "id") UUID id){
        Optional<MusicEntity> music = musicService.getById(id);
        if (music.isEmpty()) return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Music not found");
        return ResponseEntity.status(HttpStatus.OK).body(music.get());
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Deleta música", method = "DELETE")
    public ResponseEntity<Object> delete(@PathVariable(value = "id") UUID id){
        return ResponseEntity.status(HttpStatus.OK).body(musicService.delete(id));
    }

    @PutMapping("/{id}")
    @Operation(summary = "Atualiza uma música", method = "PUT")
    public ResponseEntity<Object> update(@PathVariable(value = "id") UUID id, @RequestBody @Valid MusicRequestDTO musicRequestDTO){
        return ResponseEntity.status(HttpStatus.OK).body(musicService.update(musicRequestDTO,id));
    }


}
