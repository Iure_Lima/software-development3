package com.ds3.securyusermanager.services;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.ds3.securyusermanager.models.UserEntity;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Service
public class TokenService {

    @Value("${api.security.token.secret}")
    private String secret;

    public String generateToken(UserEntity user){
        try{
            Algorithm algorithm = Algorithm.HMAC256(secret);
            String token = JWT.create().withIssuer("SecuryUserManager")
                    .withSubject(user.getLogin())
                    .withExpiresAt(generateExpirationDate()).sign(algorithm);
            return token;
        }
        catch (JWTCreationException exception){
            throw new RuntimeException("Erro while geretate token", exception);
        }
    }

    public String getUsernameOfToken(String token){
        try{
            Algorithm algorithm = Algorithm.HMAC256(secret);
            return JWT.require(algorithm).withIssuer("SecuryUserManager").build()
                    .verify(token).getSubject();
        }catch (JWTVerificationException exception){
            return "Token Invalid or Expired";
        }
    }

    public boolean validationToken(String token){
        try{
            Algorithm algorithm = Algorithm.HMAC256(secret);
             JWT.require(algorithm).build().verify(token);
             return true;
        }catch (JWTVerificationException exception){
            return false;
        }
    }

    private Instant generateExpirationDate(){
        return LocalDateTime.now().plusHours(2).toInstant(ZoneOffset.of("-03:00"));
    }
}
