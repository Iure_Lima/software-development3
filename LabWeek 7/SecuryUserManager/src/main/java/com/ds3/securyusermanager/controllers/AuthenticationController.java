package com.ds3.securyusermanager.controllers;

import com.ds3.securyusermanager.models.DTOs.AuthenticationDTO;
import com.ds3.securyusermanager.models.DTOs.RegisterDTO;
import com.ds3.securyusermanager.models.UserEntity;
import com.ds3.securyusermanager.services.TokenService;
import com.ds3.securyusermanager.services.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
@Tag(name = "SecurityAPI")
public class AuthenticationController {
    @Autowired
    TokenService tokenService;

    private AuthenticationManager authenticationManager;
    private UserService userService;

    public AuthenticationController(AuthenticationManager manager, UserService userService){
        this.authenticationManager = manager;
        this.userService = userService;
    }

    @PostMapping("/login")
    @Operation(summary = "Faz login do usuário", method = "POST")
    public ResponseEntity login(@RequestBody @Valid AuthenticationDTO data){
        var usernamePassword = new UsernamePasswordAuthenticationToken(data.login(),data.password());
        var auth = this.authenticationManager.authenticate(usernamePassword);

        var token = tokenService.generateToken((UserEntity) auth.getPrincipal());
        return ResponseEntity.status(HttpStatus.OK).body(token);
    }

    @PostMapping("/register")
    @Operation(summary = "Registra o usuário", method = "POST")
    public ResponseEntity register(@RequestBody @Valid RegisterDTO registerDTO){
        if (userService.existsUser(registerDTO.login())) return  ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

        String encryptedPassword = new BCryptPasswordEncoder().encode(registerDTO.password());
        UserEntity newUser = new UserEntity();
        BeanUtils.copyProperties(registerDTO, newUser);
        newUser.setPassword(encryptedPassword);
        userService.saveUser(newUser);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
