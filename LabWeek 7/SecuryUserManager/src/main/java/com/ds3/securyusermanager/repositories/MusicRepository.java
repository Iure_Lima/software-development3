package com.ds3.securyusermanager.repositories;

import com.ds3.securyusermanager.models.MusicEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface MusicRepository extends JpaRepository<MusicEntity, UUID> {
}
