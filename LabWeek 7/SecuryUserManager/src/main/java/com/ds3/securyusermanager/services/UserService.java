package com.ds3.securyusermanager.services;

import com.ds3.securyusermanager.models.UserEntity;
import com.ds3.securyusermanager.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {
    @Autowired
    private UserRepository repository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return repository.findByLogin(username);
    }


    public boolean existsUser(String username){
        return repository.existsByLogin(username);
    }

    public void saveUser(UserEntity user){
        repository.save(user);
    }
}
