package com.ds3.securyusermanager.models.DTOs;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record MusicRequestDTO(
        @NotNull @NotBlank String title,
        @NotNull @NotBlank String artist,
        @NotNull @NotBlank String gender,
        @NotNull  Double duration
) { }
