package com.ds3.securyusermanager.models;

import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "TB_MUSICS")
@Getter
@Setter
public class MusicEntity extends AbstractEntity {
    @Column(name ="TL_MUSIC")
    private String title;

    @Column(name="ART_MUSIC")
    private String artist;

    @Column(name="GED_MUSIC")
    private String gender;

    @Column(name="DUR_MUSIC")
    private Double duration;
}
