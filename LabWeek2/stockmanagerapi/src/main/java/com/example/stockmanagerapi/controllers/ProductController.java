package com.example.stockmanagerapi.controllers;

import com.example.stockmanagerapi.dtos.ProductRecordDto;
import com.example.stockmanagerapi.models.ProductsModel;
import com.example.stockmanagerapi.repositories.ProductsRepository;
import com.example.stockmanagerapi.services.ProductService;
import jakarta.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
public class ProductController {

    @Autowired
    ProductService productService;

    @PostMapping("/products")
    public ResponseEntity<ProductsModel> saveProduct(@RequestBody @Valid ProductRecordDto product){
        ProductsModel productModel = new ProductsModel();
        BeanUtils.copyProperties(product, productModel);
        return productService.saveProduct(productModel);
    }

    @GetMapping("/products")
    public ResponseEntity<List<ProductsModel>> getAllProducts(){
        return productService.getAllProducts();
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductByID(@PathVariable(value = "id") UUID id){
        return productService.getProductByID(id);
    }

    @GetMapping("/products/{param}/{name}")
    public ResponseEntity<Object> getProductsByParam(@PathVariable(value = "param") String param, @PathVariable(value = "name") String name){
        return productService.getProductsByParam(param,name);
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<Object> updateProduct(@PathVariable(value = "id") UUID id, @RequestBody @Valid Map<String, String> updateFields){
        return productService.updateProduct(updateFields, id);
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<Object> deleteProduct(@PathVariable(value = "id") UUID id){
        return productService.deleteProduct(id);
    }

}
