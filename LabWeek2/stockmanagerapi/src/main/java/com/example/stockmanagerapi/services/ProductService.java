package com.example.stockmanagerapi.services;

import com.example.stockmanagerapi.controllers.ProductController;
import com.example.stockmanagerapi.models.ProductsModel;
import com.example.stockmanagerapi.repositories.ProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.*;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;



@Service
public class ProductService {

    @Autowired
    ProductsRepository productsRepository;

    public ResponseEntity<ProductsModel> saveProduct(ProductsModel productsModel){
        return ResponseEntity.status(HttpStatus.CREATED).body(productsRepository.save(productsModel));
    }

    public ResponseEntity<List<ProductsModel>> getAllProducts(){
        List<ProductsModel> listProducts = productsRepository.findAll();
        if (!listProducts.isEmpty()){
            for (ProductsModel product : listProducts){
                UUID id = product.getId();
                product.add(linkTo(methodOn(ProductController.class).getProductByID(id)).withSelfRel());
            }
        }
        return ResponseEntity.status(HttpStatus.OK).body(listProducts);
    }

    public ResponseEntity<Object> getProductByID(UUID id){
        Optional<ProductsModel> product = productsRepository.findById(id);
        if (product.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Product not found.");
        }
        product.get().add(linkTo(methodOn(ProductController.class).getAllProducts()).withRel("Products List"));
        return ResponseEntity.status(HttpStatus.OK).body(product.get());
    }


    public ResponseEntity<Object> getProductsByParam(String param, String value){
        List<ProductsModel> productsList;

        try {
            switch (param.toLowerCase()) {
                case "name":
                    productsList = productsRepository.findByName(value);
                    break;
                case "price":
                    Double price = Double.parseDouble(value);
                    productsList = productsRepository.findByPrice(price);
                    break;
                case "amount":
                    Integer amount = Integer.parseInt(value);
                    productsList = productsRepository.findByAmount(amount);
                    break;
                default:
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid parameter.");
            }

            if (productsList.isEmpty()){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Product not found.");
            }

            for (ProductsModel product : productsList){
                UUID id = product.getId();
                product.add(linkTo(methodOn(ProductController.class).getProductByID(id)).withSelfRel());
            }

            return ResponseEntity.status(HttpStatus.OK).body(productsList);
        } catch (NumberFormatException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid value format.");
        }
    }

    public ResponseEntity<Object> updateProduct(Map<String, String> updateFields, UUID id){
        Optional<ProductsModel> product = productsRepository.findById(id);
        if (product.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Product not found.");
        }
        ProductsModel productsModel = product.get();

        if(updateFields.containsKey("name")){
            productsModel.setName(updateFields.get("name"));
        }

        if(updateFields.containsKey("description")){
            productsModel.setDescription(updateFields.get("description"));
        }

        if(updateFields.containsKey("price")){
            try{
                productsModel.setPrice(Double.parseDouble(updateFields.get("price")));
            }catch (Exception e){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid parameter.");
            }
        }

        if(updateFields.containsKey("amount")){
            try{
                productsModel.setAmount(Integer.parseInt(updateFields.get("amount")));
            }catch (Exception e){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid parameter.");
            }
        }

        productsModel.setUpdatedAt(new Date());

        return ResponseEntity.status(HttpStatus.OK).body(productsRepository.save(productsModel));
    }

    public ResponseEntity<Object> deleteProduct(UUID id){
        Optional<ProductsModel> product = productsRepository.findById(id);
        if (product.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Product not found.");
        }
        productsRepository.delete(product.get());

        return ResponseEntity.status(HttpStatus.OK).body("Product deleted");
    }
}
