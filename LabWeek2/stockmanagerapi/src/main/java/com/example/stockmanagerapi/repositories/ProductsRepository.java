package com.example.stockmanagerapi.repositories;

import com.example.stockmanagerapi.models.ProductsModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ProductsRepository extends JpaRepository<ProductsModel, UUID> {
    @Query("SELECT p FROM ProductsModel p WHERE p.Name LIKE :label%")
    List<ProductsModel> findByName(@Param("label") String label);

    @Query("SELECT p FROM ProductsModel p WHERE p.Price = ?1")
    List<ProductsModel> findByPrice(Double price);

    @Query("SELECT p FROM ProductsModel p WHERE p.Amount = ?1")
    List<ProductsModel> findByAmount(Integer Amount);
}

