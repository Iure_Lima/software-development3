package com.example.stockmanagerapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StockmanagerapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(StockmanagerapiApplication.class, args);
	}

}
