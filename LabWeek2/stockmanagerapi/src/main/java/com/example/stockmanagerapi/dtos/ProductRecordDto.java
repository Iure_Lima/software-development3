package com.example.stockmanagerapi.dtos;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record ProductRecordDto(@NotBlank String name, @NotBlank String description, @NotNull Double price, @NotNull Integer amount) {

}
