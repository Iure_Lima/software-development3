package com.example.stockmanagerapi.models;

import jakarta.persistence.*;
import org.springframework.hateoas.RepresentationModel;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@MappedSuperclass
public abstract class AbstractEntity extends RepresentationModel<AbstractEntity> implements Serializable {

    AbstractEntity(){
        Date now = new Date();
        CreatedAt = now;
        UpdatedAt = now;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "ID")
    private UUID Id;
    @Column(name = "DT_CREATED_AT")
    private final Date CreatedAt;
    @Column(name = "DT_UPDATED_AT")
    private Date UpdatedAt;

    public UUID getId() {
        return Id;
    }

    public Date getCreatedAt() {
        return CreatedAt;
    }

    public Date getUpdatedAt() {
        return UpdatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        UpdatedAt = updatedAt;
    }
}
