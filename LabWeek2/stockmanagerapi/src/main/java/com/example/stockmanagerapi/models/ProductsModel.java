package com.example.stockmanagerapi.models;

import jakarta.persistence.*;


@Entity
@Table(name = "TB_PRODUCTS")
public class ProductsModel extends AbstractEntity{

    @Column(name = "NM_PRODUCT")
    private String Name;

    @Column(name = "DC_PRODUCT")
    private String Description;

    @Column(name = "VL_PRODUCT")
    private Double Price;

    @Column(name = "AM_PRODUCT")
    private Integer Amount;


    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public Double getPrice() {
        return Price;
    }

    public void setPrice(Double price) {
        Price = price;
    }

    public Integer getAmount() {
        return Amount;
    }

    public void setAmount(Integer amount) {
        Amount = amount;
    }
}
